import React, { Component } from 'react';
import '../App.css';
import AddInput from "../components/addInput/AddInput";
import Movies from "../components/movies/Movies";

class App extends Component {
  state = {
    filmNames: [],
    currentName: ''
  }

  changeCurrentName = (event) => {
    const currentName = event.target.value
    this.setState({currentName})
}

  addFilmName = () => {
    const movies = [...this.state.filmNames]
    movies.push({name: this.state.currentName, id: Date.now()});
    this.setState({filmNames: movies})
  }

  deleteFilmName = (id) => {
      const movies = [...this.state.filmNames];
      const deletingItem = movies.findIndex((movie) => movie.id === id);
      movies.splice(deletingItem, 1);
      this.setState({filmNames: movies})
  }


  render() {
    return (
      <div className="App">
        <AddInput
            change={this.changeCurrentName}
            clicked={this.addFilmName}

        />
        <Movies movies={this.state.filmNames}
                clicked={this.deleteFilmName}
        />
      </div>
    );
  }
}

export default App;

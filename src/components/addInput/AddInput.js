import React from 'react';

const AddInput = props => {
    return (
        <div>
            <input onChange={props.change} type="text" />
            <button onClick={props.clicked}>Add</button>
        </div>
    )
}
export default AddInput;
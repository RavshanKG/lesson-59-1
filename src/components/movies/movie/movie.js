import React from 'react';

const Movie = props => {
    return (
        <div>
            <input value={props.movie} type="text"/>
            <span onClick={props.clicked}><a href="#">x</a></span>
        </div>
    )
}
export default Movie;
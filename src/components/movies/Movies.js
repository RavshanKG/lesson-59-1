import React from 'react';
import Movie from "./movie/movie";

const Movies = props => {
    return (
        <div>
            {props.movies.map((movie) => < Movie clicked={() => props.clicked(movie.id)} movie={movie.name} key={movie.id}/> )}
        </div>
    )
}
export default Movies;
